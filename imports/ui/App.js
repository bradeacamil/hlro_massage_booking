import React from 'react';
import { Helmet } from "react-helmet";
import Responsive from 'react-responsive';

import Header from './Header';
import AddBooking from './AddBooking';
import BookingList from './BookingList';

const Desktop = props => <Responsive {...props} minWidth={992} />;
const Mobile = props => <Responsive {...props} maxWidth={991} />;

export default class App extends React.Component {
    componentDidMount() {
    }
    render() {
        return (
            <div style={styles}>
                <Helmet>
                    <meta name="viewport" content="width=device-width" />
                    <link rel="manifest" href="/public/app_manifest.json" />
                </Helmet>

                <Desktop>
                    <Header />
                </Desktop>

                <Mobile>
                    <center>
                        <Header />
                    </center>
                </Mobile>

                <BookingList bookingsList={this.props.bookingsList} loggedInUser={this.props.loggedInUser} />
            </div>
        );
    }
}

const styles = {
    display: 'flex',
    flexDirection: 'column'
}