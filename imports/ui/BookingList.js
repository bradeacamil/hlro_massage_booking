import React from 'react';

import BigCalendar from 'react-big-calendar'
import '../../styles/react-big-calendar.css';
import moment from 'moment';
import 'moment/locale/en-gb';

import Responsive from 'react-responsive';
import Swipeable from 'react-swipeable';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
  } from 'material-ui/Dialog';
import 'typeface-roboto';

import Booking from './Booking';
import { Bookings } from './../api/bookings';

const Desktop = props => <Responsive {...props} minWidth={768} />;
const Mobile = props => <Responsive {...props} maxWidth={767} />;

export default class BookingList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isBooked: false,
            itemToBeDeleted: {},
            open: false,
            openName: false,
            name: '',
            start: '',
            end: ''
        };
    }

    handleClickOpen = () => {
        this.setState({ open: true });
    };
    
    handleClose = () => {
        this.setState({ open: false });
    };

    handleCloseNameModal = () => {
        this.setState({ openName: false });
    };

    handleNameChange = (e) => {
        this.setState({ name: e.target.value });
    }

    handleOk = () => {
        Bookings.insert({
            title: this.state.name,
            start: this.state.start,
            end: this.state.end
        })

        this.setState({ openName: false });
    }

    handleDelete = () => {
        Bookings.remove(this.state.itemToBeDeleted._id);
        this.setState({ open: false });
        this.setState({ itemToBeDeleted: {} });
    }

    getElementsByText(str, tag) {
        return Array.prototype.slice.call(document.getElementsByTagName(tag)).filter(el => el.textContent.trim() === str.trim());
    }

    getUser() {
        if (this.props.loggedInUser)
            return `${this.props.loggedInUser.split(',')[1].trim(' ')} ${this.props.loggedInUser.split(',')[0].trim(' ')}`;
    }

    // handleSwipeLeft = () => {
    //     this.getElementsByText('next', 'button').click();
    // }

    render() {
        //console.log(this.props)
        BigCalendar.setLocalizer(BigCalendar.momentLocalizer(moment));
        return (
            <div>
                <Swipeable onSwipingLeft={this.handleSwipeLeft}>
                <Desktop style={calendarMarginStyle}>
                    <BigCalendar
                        selectable
                        messages={{
                            allDay: ''
                        }}
                        events={this.props.bookingsList}
                        defaultDate={moment().toDate()}
                        min={moment('2018-02-23 08:00:00').toDate()}
                        max={moment('2018-02-23 20:00:00').toDate()}
                        defaultView={'work_week'}
                        views={['day', 'work_week']}
                        onSelectEvent={event => {
                                this.setState({ isBooked: true });
                                //console.log('stergem?');
                                this.setState({ itemToBeDeleted: event });
                                this.handleClickOpen();
                            }
                        }
                        onSelectSlot={slotInfo => {
                            //console.log(slotInfo);
                            this.setState({ isBooked: false });
                            this.props.bookingsList.map((booking) => {
                                if(moment(slotInfo.start).diff(moment(booking.start)) === 0) {
                                    this.setState({ isBooked: true });
                                }
                            });

                            if (slotInfo.action === 'doubleClick') 
                                if (this.state.isBooked === false && moment().diff(slotInfo.start, 'minutes') <= 29) {
									if (this.getUser()) {
										Bookings.insert({
											title: this.getUser(),
											start: slotInfo.start.toString(),
											end: slotInfo.end.toString()
										})
									}
									else {
										this.setState({ openName: true });
										this.setState({ start: slotInfo.start.toString() })
										this.setState({ end: slotInfo.end.toString() })
									}
                                }
                            }
                        }
                    />
                </Desktop>
                <Mobile  style={{height: '250vw', fontSize: '125%'}}>
                    <BigCalendar
                        selectable
                        longPressThreshold={100}
                        messages={{
                            allDay: ''
                        }}
                        events={this.props.bookingsList}
                        defaultDate={moment().toDate()}
                        min={moment('2018-02-23 08:00:00').toDate()}
                        max={moment('2018-02-23 20:00:00').toDate()}
                        defaultView={'day'}
                        views={['day']}
                        onSelectEvent={event => {
                                this.setState({ isBooked: true });
                                //console.log('stergem?');
                                this.setState({ itemToBeDeleted: event });
                                this.handleClickOpen();
                            }
                        }
                        onSelectSlot={slotInfo => {
                            //console.log(slotInfo);
                            this.setState({ isBooked: false });
                            this.props.bookingsList.map((booking) => {
                                if(moment(slotInfo.start).diff(moment(booking.start)) === 0) {
                                    this.setState({ isBooked: true });
                                }
                            });

                            if (slotInfo.action === 'select' || slotInfo.action === 'doubleClick') 
                                if (this.state.isBooked === false && moment().diff(slotInfo.start, 'minutes') <= 29) {
									if (this.getUser()) {
										Bookings.insert({
											title: this.getUser(),
											start: slotInfo.start.toString(),
											end: slotInfo.end.toString()
										})
									}
									else {
										this.setState({ openName: true });
										this.setState({ start: slotInfo.start.toString() })
										this.setState({ end: slotInfo.end.toString() })
									}
                                }
                            }
                        }
                    />
                </Mobile>
                <div>
                    <Dialog
                        open={this.state.open}
                        onClose={this.handleClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">
                            {"Confirm deletion"}
                        </DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                Are you sure you want to delete this booking ?
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary" autoFocus>
                                No
                            </Button>
                            <Button onClick={this.handleDelete} color="secondary">
                                Yes
                            </Button>
                        </DialogActions>
                    </Dialog>
                    <Dialog
                        open={this.state.openName}
                        onClose={this.handleCloseNameModal}
                        aria-labelledby="form-dialog-title"
                        >
                        <DialogTitle id="form-dialog-title">Enter name</DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                            Please enter your name ...
                            </DialogContentText>
                            <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Name"
                            type="text"
                            fullWidth
                            onChange={this.handleNameChange}
                            onKeyPress={event => {
                                if (event.key === 'Enter') {
                                    this.handleOk()
                                }
                            }}
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleCloseNameModal} color="secondary">
                            Cancel
                            </Button>
                            <Button onClick={this.handleOk} color="primary">
                            OK
                            </Button>
                        </DialogActions>
                    </Dialog>
                </div>
                </Swipeable>
            </div>
        );
    }
}

const calendarMarginStyle = {
    margin : "1vw"
};