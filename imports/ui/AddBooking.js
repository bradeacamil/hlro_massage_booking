import React from 'react';

import { Bookings } from './../api/bookings';

//NOT USED ANYMORE

export default class AddBooking extends React.Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit = (e) => {
        e.preventDefault();

        let booking = e.target.booking.value;

        if (booking) {
            e.target.booking.value = '';

            Bookings.insert({
                title: 'username',
                start: moment('2018-02-22 02:30:00').toDate(),
                end: moment('2018-02-22 03:00:00').toDate(),
            });
        }
    };

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <input type="text" name="booking" placeholder="Booking ..." />
                <button>Add booking</button>
            </form>
        );
    }
}