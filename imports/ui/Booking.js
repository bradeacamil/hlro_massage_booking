import React from 'react';

import { Bookings } from './../api/bookings';

export default class Booking extends React.Component {
    componentDidMount() {
    }

    render() {
        return (
            <p key={this.props.bookingId}>
                {this.props.bookingTime}
                <br/>
                {this.props.bookingUser}
            </p>
        );
    }
}