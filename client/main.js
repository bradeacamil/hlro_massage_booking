import React from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';

import moment from 'moment';
import 'moment/locale/en-gb';
import _ from 'lodash';

import { Bookings } from './../imports/api/bookings';
import App from './../imports/ui/App';

let bookings = [];
const user = {};

Meteor.startup(() => {
    // if (Meteor.isClient) 
    //     if (!Meteor.user()) {
	// 		console.log('logging in ...');
	// 		Meteor.loginWithAzureAd();
	// 	}
    
    Tracker.autorun(() => {
        navigator.serviceWorker.register('/sw.js')
            .then() 
			.catch(error => console.log('ServiceWorker registration failed: ', error));
			
		if (!Meteor.user()) {
			console.log('logging in ...');
			Meteor.loginWithAzureAd();
		}

        if (navigator.onLine) {
            bookings = []

            let db = Bookings.find({}, {}).fetch();

            // console.log(_.uniqBy(db, 'start'))

            bookings = db

            _.uniqBy(bookings, '_id');

            for (const booking of bookings) {
                booking.start = moment(booking.start).toDate();
                booking.end = moment(booking.end).toDate();
            }

            localStorage.removeItem('hlroMassageBookings');
            localStorage.setItem('hlroMassageBookings', JSON.stringify(bookings));

            console.log(bookings)

            // console.log('local', JSON.parse(localStorage.getItem('hlroMassageBookings')))
        }
        else {
            bookings = []

            let local = JSON.parse(localStorage.getItem("hlroMassageBookings"))|| [];
            let db = Bookings.find({}, {}).fetch();

            // console.log(db);
            // console.log(local);

            // console.log('bookings', bookings);

            // for (const item of local) {
            //     for(const booking of bookings) {
            //         let exists = false;

            //         console.log('item', item)
            //         console.log('booking', booking)

            //         if (booking._id === item._id) {
            //             exists = true;
            //         }

            //         if (exists === false) {
            //             bookings.push(item)
            //         }
            //     }
            // }

            bookings = db.concat(bookings);

            _.uniqBy(bookings, '_id');

            // console.log('bookings uniq', bookings);

            for (const booking of bookings) {
                booking.start = moment(booking.start).toDate();
                booking.end = moment(booking.end).toDate();
            }
		}

		console.log('Meteor user', Meteor.user())

        ReactDOM.render(
            <App 
                bookingsList={bookings} 
                loggedInUser={
                    Meteor.user() ? Meteor.user().profile.name : Meteor.user()
                }
            />, 
			document.getElementById('app'));
    });
});
